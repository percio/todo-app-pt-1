import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };

  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }

  handleCheck = (checkId) =>{
    const newTodos =this.state.todos.map(
      todoItem =>{
        if(todoItem.id === checkId){
          todoItem.completed = !todoItem.completed;
        }
        return todoItem;
      }
    )
    this.setState({ todos: newTodos })
  }

  handleDeleteComplete = () =>{
    const newTodos = this.state.todos.filter(
     todoItem => todoItem.completed !== true
    )
     this.setState({ todos: newTodos })
  }

  handleText=(event)=>{
    this.setState({value:event.target.value})

  }
  
  handleAddTodo = ()=>{
    const newTodo = {
      "userId": 1,
      "id": uuidv4(),
      "title": this.state.value,
      "completed": false
    }
    const newTodos = [newTodo,...this.state.todos]
  
    this.setState({todos:newTodos, value:''})
  }

  handleSubmit = (event)=>{
    if(event.key ==='Enter'){
      this.handleAddTodo()
    }

  }



  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          value={this.state.value}
          type="text"
          onChange={this.handleText }
          className="new-todo" 
          placeholder="What needs to be done?"
          onKeyDown={this.handleSubmit}
           autoFocus />
        </header>
        <TodoList handleCheck={this.handleCheck} todos={this.state.todos} handleDelete={this.handleDelete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleDeleteComplete} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed} 
           onChange={()=>this.props.handleCheck(this.props.id)} 
          />
          <label>{this.props.title}</label>
          <button 
          className="destroy" 
            onClick={()=>this.props.handleDelete(this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem handleCheck={this.props.handleCheck} handleDelete={this.props.handleDelete} title={todo.title} completed={todo.completed} key={todo.id} id={todo.id}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
