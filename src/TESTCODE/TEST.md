import React, { useState } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";



const App = () => {
  return (
    <div>
      <CreateTodoList />
    </div>
  );
};


const CreateTodoList = () => {
  const [todos, setTodos] = useState(todosList);
  const [newValue, setNewValue] = useState("");

  const handleSubmit = (ev) => {
    ev.preventDefault();
    if (!newValue) return;
    addTodo({ newValue });
    console.log(newValue)
    setNewValue("");
  };

  const addTodo = () => {
    const NewTodos = [{ userId: 1, id: { uuidv4 }, title: {newValue}, completed: false }];
    setTodos([NewTodos, ...todos]);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            value={newValue}
            onChange={(ev) => {
              setNewValue(ev.target.value);
            }}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
          />
        </form>
      </header>
      <TodoList todos={todos} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed">Clear completed</button>
      </footer>
    </section>
  );
};







const TodoList = (props) => {
 
  return (
    
    <section className="main">
    
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem title={todo.title} completed={todo.completed} key={uuidv4()} />
        ))}
      </ul>
    </section>
  );
};







const TodoItem = (props) => {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input className="toggle" type="checkbox" checked={props.completed} />
        <label>{props.title}</label>
        <button className="destroy" />
      </div>
    </li>
  );
};

export default App;
